extends Node2D


enum Entity {
	LASER,
	BLOCKER,
	RECEIVER,
	WALL
}


var laser_scene := load("res://src/Laser.tscn")
var blocker_scene := load("res://src/Blocker.tscn")
var receiver_scene := load("res://src/Receiver.tscn")
var wall_scene := load("res://src/Wall.tscn")

var radius := 200
var sectors := 12
var ring := {
	0: Entity.LASER,
	1: Entity.LASER,
	5: Entity.BLOCKER,
	6: Entity.RECEIVER,
	7: Entity.LASER
}
var sector_angle: float;
var is_active := false


func ready() -> void:
	rotation = PI
	sector_angle = 2.0 * PI / sectors
	for i in range(0, sectors):
		if ring.has(i):
			var position = Vector2(0, radius)
			position = position.rotated(sector_angle * i)
			var angle = 1.5 * PI + sector_angle * i
			
			match ring.get(i):
				Entity.LASER:
					var laser = laser_scene.instance()
					add_child(laser)
					laser.position = position
					laser.rotation = angle
				Entity.BLOCKER:
					var blocker = blocker_scene.instance()
					add_child(blocker)
					blocker.position = position
					blocker.rotation = angle
				Entity.RECEIVER:
					var receiver = receiver_scene.instance()
					add_child(receiver)
					receiver.position = position
					receiver.rotation = angle
				Entity.WALL:
					var wall = wall_scene.instance()
					add_child(wall)
					wall.position = position
					wall.rotation = angle


func _physics_process(delta: float) -> void:
	if is_active:
		if Input.is_action_just_pressed("ui_right"):
			rotate(sector_angle)
		if Input.is_action_just_pressed("ui_left"):
			rotate(-sector_angle)


func _draw() -> void:
	var color = Color.yellow if is_active else Color.gray
	draw_arc(Vector2.ZERO, radius, 0, 2 * PI, 100, color, 3)


func set_is_active(flag: bool) -> void:
	is_active = flag
	update()
	if is_active:
		$Activate.play()


func rotate(angle: float) -> void:
	if !$Tween.is_active():
		$Rotate.play()
		$Tween.interpolate_property(
			self,
			"rotation",
			rotation,
			rotation + angle,
			0.1,
			Tween.TRANS_QUAD,
			Tween.EASE_IN_OUT
		)
		$Tween.start()
