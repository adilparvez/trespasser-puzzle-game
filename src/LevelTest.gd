extends Node2D


signal solved


enum Entity {
	LASER,
	BLOCKER,
	RECEIVER
}


var ring_scene := load("res://src/Ring.tscn")

var sectors := 12
var rings := [
	{
		3: Entity.LASER
	},
	{
		5: Entity.LASER
	},
	{
		7: Entity.LASER
	},
	{
		0: Entity.RECEIVER,
		4: Entity.RECEIVER,
		8: Entity.RECEIVER
	}
]


var current_ring_index := rings.size() - 2
var max_ring_index = rings.size() - 2
var ring_nodes := []


func _ready() -> void:
	for i in range(rings.size()):
		var ring = ring_scene.instance()
		add_child(ring)
		ring_nodes.append(ring)
		ring.radius = (i + 1) * 75
		ring.ring = rings[i]
		ring.ready()
		if i == current_ring_index:
			ring.is_active = true


func _physics_process(delta: float) -> void:
	if Input.is_action_just_pressed("ui_up"):
		if current_ring_index < max_ring_index:
			ring_nodes[current_ring_index].set_is_active(false)
			current_ring_index += 1
			ring_nodes[current_ring_index].set_is_active(true)
	if Input.is_action_just_pressed("ui_down"):
		if current_ring_index > 0:
			ring_nodes[current_ring_index].set_is_active(false)
			current_ring_index -= 1
			ring_nodes[current_ring_index].set_is_active(true)

	if are_all_receivers_active():
		emit_signal("solved")


func are_all_receivers_active() -> bool:
	var receivers = get_tree().get_nodes_in_group("receivers")
	for receiver in receivers:
		if !receiver.is_active():
			return false
	
	return true
