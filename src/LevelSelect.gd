extends Control

const NUM_LEVELS := 6#14


func _ready() -> void:
	for i in range(1, NUM_LEVELS + 1):
		var button = Button.new()
		button.text = "Level {i}".format({ "i": i })
		button.connect("pressed", self, "select_level", [i])
		$Levels.add_child(button)


func select_level(i: int) -> void:
	get_tree().change_scene("res://src/Levels/Level{i}.tscn".format({ "i": i }))
