extends Node2D


enum Entity {
	LASER,
	BLOCKER,
	RECEIVER,
	WALL
}


var ring_scene := load("res://src/Ring.tscn")

var sectors := 12
var rings := [
	{
		3: Entity.LASER
	},
	{
		5: Entity.LASER
	},
	{
		7: Entity.LASER
	},
	{
		0: Entity.RECEIVER,
		1: Entity.WALL,
		2: Entity.WALL,
		3: Entity.WALL,
		4: Entity.RECEIVER,
		5: Entity.WALL,
		6: Entity.WALL,
		7: Entity.WALL,
		8: Entity.RECEIVER,
		9: Entity.WALL,
		10: Entity.WALL,
		11: Entity.WALL
	}
]


var current_ring_index := rings.size() - 2
var max_ring_index = rings.size() - 2
var ring_nodes := []


func _ready() -> void:
	$Label.hide()
	for i in range(rings.size()):
		var ring = ring_scene.instance()
		add_child(ring)
		ring_nodes.append(ring)
		ring.radius = (i + 1) * 75
		ring.ring = rings[i]
		ring.sectors = sectors
		ring.ready()
		if i == current_ring_index:
			ring.is_active = true


func _physics_process(delta: float) -> void:
	if Input.is_action_just_pressed("ui_up"):
		if current_ring_index < max_ring_index:
			ring_nodes[current_ring_index].set_is_active(false)
			current_ring_index += 1
			ring_nodes[current_ring_index].set_is_active(true)
	if Input.is_action_just_pressed("ui_down"):
		if current_ring_index > 0:
			ring_nodes[current_ring_index].set_is_active(false)
			current_ring_index -= 1
			ring_nodes[current_ring_index].set_is_active(true)

	if are_all_receivers_active():
		$Label.show()
		for node in ring_nodes:
			node.is_active = false
		yield(get_tree().create_timer(2.0), "timeout")
		get_tree().change_scene("res://src/LevelSelect.tscn")


func are_all_receivers_active() -> bool:
	var receivers = get_tree().get_nodes_in_group("receivers")
	for receiver in receivers:
		if !receiver.is_active():
			return false
	
	return true
