extends Node2D


var _active := false


func _on_Area2D_area_entered(area: Area2D) -> void:
	_active = true


func _on_Area2D_area_exited(area: Area2D) -> void:
	_active = false


func is_active() -> bool:
	return _active
