extends Node2D


const MAX_LENGTH := 2000
const DEFAULT_COLOR := Color.goldenrod
const RECEIVER_COLOR := Color.mediumseagreen
const BLOCKER_COLOR := Color.orangered


func _physics_process(delta: float) -> void:
	$RayCast2D.cast_to = $RayCast2D.cast_to.normalized() * MAX_LENGTH

	if $RayCast2D.is_colliding():
		$End.global_position = $RayCast2D.get_collision_point()

		var collider = $RayCast2D.get_collider()
		match collider.get_collision_layer():
			1: self.modulate = DEFAULT_COLOR
			2: self.modulate = BLOCKER_COLOR
			4: self.modulate = RECEIVER_COLOR
	else:
		$End.global_position = $RayCast2D.cast_to
		self.modulate = DEFAULT_COLOR

	$Beam.region_rect.end.x = $End.position.length()
